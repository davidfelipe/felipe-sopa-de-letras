<!DOCTYPE html>
<html>
<head>
	<title>Sopa de letras</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
</head>
<body>

<div class="container-fluid">
<div class="row">
  <div class="col-md-6 offset-md-3">

    <div class="jumbotron">
      <h1 class="display-3">SOPA DE LETRAS</h1>
      <p class="lead">Introduzca la cantidad de palabras que desea en la sopa de letras</p>
      <hr class="my-4">
      


      <form action="index2.php" method="POST" >
        <div class="form-group">
          <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="cantidad" placeholder="Cantidad de palabras">
        </div>

        <p class="lead">
        <button type="submit" class="btn btn-primary btn-lg" href="#" role="button">Siguiente </button>
      </p>

    </form>
      

    </div>
</div>
</div>
</div>

  <script type="text/javascript" src="js/jquery.js" ></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>