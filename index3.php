<!DOCTYPE html>
<html>
<head>
	<title>David</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
</head>
<body>

<div class="container-fluid">
<div class="row">
  <div class="col-xs-10 ">

	    <div class="jumbotron">
	      
		      <h1 class="display-3">SOPA DE LETRAS</h1>
		      <p class="lead">Busca las siguientes palabras</p>
		      <hr class="my-4">
	      
	      		<?php require 'sopa.php'; ?>
	      		

		</div>
   </div>
    <div class="col-md-2">

	    <div class="jumbotron" style="position: fixed;">
	      <h4>Palabras</h4>
	      <hr class="my-4">

	      <?php 
	      for ($k=0; $k <$Cantidad ; $k++) { 
	      	# code...
	     
	      	echo '<p class="lead">'.strtoupper($Diccionario["Palabra".$k]["palabra"]).'</p>'; 
	       }
	      ?>
	      

		</div>
   </div>

</div>
</div>

  <script type="text/javascript" src="js/jquery.js" ></script>
  <script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>






